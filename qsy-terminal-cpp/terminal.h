#pragma once
#include <boost/asio.hpp>
#include <map>
#include <tuple>
#include <mutex>
#include "qsy_packet.h"
#include "node_finder.h"
#include "blocking_queue.h"
#include "node.h"

namespace qsy {
    class terminal {
        enum class state {discovery, standby, running, exiting};
        node_finder m_finder;
        blocking_queue<packet> m_incoming_packets;
        blocking_queue<std::pair<uint16_t, boost::asio::ip::address>> m_incoming_nodes;
        boost::asio::io_service m_io_serv;
		std::mutex m_nodes_mutex;
		std::map<uint16_t, node> nodes;
        std::atomic<bool> m_running;
        std::thread m_worker;
		std::thread m_io_serv_worker;

        state m_state;
        void register_nodes();
        void standby();
		void execute_keepalive();
        void run();
    public:
        terminal();
        ~terminal();
		void send_command(uint16_t phy_id,
					      packet::color_t color = { 0,0,0 },
					      uint32_t delay = 0);
        void find_nodes();
        void find_nodes_stop();
		void drop_nodes();
    };
}
#include "node.h"

namespace qsy {

    const std::chrono::duration<float> node::keepalive_threshold(1.5);

    node::node(uint16_t phy_id,
        const boost::asio::ip::tcp::endpoint &node_endpoint,
        blocking_queue<packet>& incoming_queue,
        boost::asio::io_service &io_serv)
        : phy_id(phy_id), m_keepalive_up(false), m_socket(io_serv), m_packet_queue(incoming_queue)
    {
        m_socket.open(node_endpoint.protocol());
		m_socket.set_option(boost::asio::ip::tcp::no_delay(true));
        m_socket.connect(node_endpoint);
    }

    bool node::keepalive()
    {
        auto now = std::chrono::steady_clock::now();
        bool alive;
        if (m_keepalive_up) 
            alive = now - m_last_keepalive < keepalive_threshold;
        else
            alive = m_keepalive_up = true;

        m_last_keepalive = now;
        return alive;
    }

    void node::reset_keepalive()
    {
        m_keepalive_up = false;
    }

	void node::send_command(packet::color_t color, uint32_t delay)
	{
		packet packet(packet::packet_type::command, phy_id, color, delay);
		m_socket.send(boost::asio::buffer(packet.get_data()));
	}

    node::~node() {
    }
}
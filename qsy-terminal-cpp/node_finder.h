#pragma once
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>
#include <array>
#include <thread>
#include "qsy_packet.h"
#include "blocking_queue.h"

namespace qsy
{
    class node_finder
    {
        static const short multicast_port = 3000;
        static const boost::asio::ip::address multicast_address;
        boost::asio::ip::udp::endpoint m_sender_endpoint;
        boost::asio::io_service m_io_serv;
        boost::asio::ip::udp::socket m_socket;
        blocking_queue<std::pair<uint16_t, boost::asio::ip::address>> &m_queue;
        std::array<char, packet::PACKET_SIZE> data;
        void handle_receive_from(const boost::system::error_code& error,
            size_t bytes_recvd);
        std::unique_ptr<std::thread> m_worker;
    public:
        explicit node_finder(blocking_queue<std::pair<uint16_t, boost::asio::ip::address>> &addr_queue);
        void start();
        void stop();
        virtual ~node_finder();
    };
}
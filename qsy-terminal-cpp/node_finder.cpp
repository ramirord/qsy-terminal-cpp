#include <iostream>
#include "node_finder.h"
#include "boost/bind.hpp"
#include "qsy_packet.h"

namespace qsy
{

    const boost::asio::ip::address node_finder::multicast_address = boost::asio::ip::address::from_string("224.0.0.12");


    void node_finder::handle_receive_from(const boost::system::error_code & error, size_t bytes_recvd)
    {
        if (!error) {
            if (bytes_recvd == packet::PACKET_SIZE) {
                packet packet(data);
				if (packet.get_type() == packet::packet_type::hello)
					m_queue.push(std::make_pair(0,m_sender_endpoint.address()));
				else
					std::cerr << "Wrong packet type on multicast!" << std::endl;
                
            }
            m_socket.async_receive_from(
                boost::asio::buffer(data, packet::PACKET_SIZE), m_sender_endpoint,
                boost::bind(&node_finder::handle_receive_from, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
        } else {
            throw error;
        }
    }


    node_finder::node_finder(blocking_queue<std::pair<uint16_t, boost::asio::ip::address>> &addr_queue)
        :
        m_io_serv(),
        m_socket{ m_io_serv },
        m_queue{ addr_queue }
    {
        auto listen_address = boost::asio::ip::address::from_string("0.0.0.0");
        boost::asio::ip::udp::endpoint listen_endpoint(listen_address, multicast_port);
        boost::system::error_code ec;

        m_socket.open(listen_endpoint.protocol());
        m_socket.set_option(boost::asio::ip::udp::socket::reuse_address(true));
        m_socket.bind(listen_endpoint, ec);
    }

    void node_finder::start()
    {
        m_io_serv.reset();
        m_socket.set_option(
            boost::asio::ip::multicast::join_group(multicast_address.to_v4()));

        m_socket.async_receive_from(
            boost::asio::buffer(data, packet::PACKET_SIZE), m_sender_endpoint,
            boost::bind(&node_finder::handle_receive_from, this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred));
        m_worker = std::make_unique<std::thread>(
            std::thread(boost::bind(&boost::asio::io_service::run, &m_io_serv)));
    }

    void node_finder::stop()
    {
        m_socket.set_option(
            boost::asio::ip::multicast::leave_group(multicast_address.to_v4()));
        m_io_serv.stop();
        m_worker->join();
    }

    node_finder::~node_finder()
    {
    }

}
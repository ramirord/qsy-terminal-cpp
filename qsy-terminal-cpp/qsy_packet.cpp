#include "qsy_packet.h"
#include <cstdint>

#define HELLO_MSG       0
#define CMD_MSG         1
#define TOUCHE_MSG      2
#define KEEP_ALIVE_MSG  3

#define RED_MASK    0x000F
#define GREEN_MASK  0x00F0
#define BLUE_MASK   0x0F00
#define WHITE_MASK  0xF000

#define RED_COMPONENT(x)   (x)     & 0xF
#define GREEN_COMPONENT(x) (x)>>4  & 0xF
#define BLUE_COMPONENT(x)  (x)>>8  & 0xF
#define WHITE_COMPONENT(x) (x)>>12 & 0xF

#define COLOR(r,g,b,w) (  (r)        & RED_MASK  ) | \
(((g) << 4) & GREEN_MASK) | \
    (((b) << 8) & BLUE_MASK) | \
    (((w) << 12) & WHITE_MASK) 

#pragma pack(push,1)
struct qsy_message {
	char signature[3];
	uint8_t type;
	uint16_t id;
	uint16_t color;
	uint32_t delay;
};
#pragma pack(pop)

namespace qsy {

	packet::packet(const std::array<char, PACKET_SIZE>& raw_data)
	{
		qsy_message raw_msg;
		std::memcpy(&raw_msg, raw_data.data(), PACKET_SIZE);

		if (raw_data[0] != 'Q' || raw_data[1] != 'S' || raw_data[2] != 'Y')
			throw - 1;
		raw_msg.id = ntohs(raw_msg.id);
		raw_msg.color = ntohs(raw_msg.color);
		raw_msg.delay = ntohl(raw_msg.delay);

		m_id = raw_msg.id;
		switch (raw_msg.type) {
		case HELLO_MSG:
			this->m_type = packet_type::hello;
			break;
		case CMD_MSG:
			m_type = packet_type::hello;
			break;
		case TOUCHE_MSG:
			m_type = packet_type::hello;
			break;
		case KEEP_ALIVE_MSG:
			m_type = packet_type::hello;
			break;
		default:
			throw - 1;
		}
		m_color = color_t(RED_COMPONENT(raw_msg.color),
			GREEN_COMPONENT(raw_msg.color),
			BLUE_COMPONENT(raw_msg.color));
		m_delay = raw_msg.delay;

		this->m_data = raw_data;

	}

	packet::packet(packet_type type,
		uint16_t id,
		color_t &c,
		uint32_t delay)
		: m_type{ type }, m_id{ id }, m_color{ c }, m_delay{ delay }, m_data{}
	{
		qsy_message raw_msg;
		switch (type) {
		case packet_type::hello:
			raw_msg.type = HELLO_MSG;
			break;
		case packet_type::keepalive:
			raw_msg.type = KEEP_ALIVE_MSG;
			break;
		case packet_type::touche:
			raw_msg.type = TOUCHE_MSG;
			break;
		case packet_type::command:
			raw_msg.type = CMD_MSG;
			break;
		}
		raw_msg.id = htons(id);
		raw_msg.color = COLOR(m_color.r, m_color.g, m_color.b, 0);
		raw_msg.color = htons(raw_msg.color);
		raw_msg.delay = htonl(delay);
		raw_msg.signature[0] = 'Q';
		raw_msg.signature[1] = 'S';
		raw_msg.signature[2] = 'Y';
		std::memcpy(m_data.data(), &raw_msg, PACKET_SIZE);
	}

	const packet packet::POISON_PILL(packet_type::poison_pill);
}

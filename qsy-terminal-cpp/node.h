#pragma once
#include <cstdint>
#include <boost/asio.hpp>
#include <chrono>
#include "blocking_queue.h"
#include "qsy_packet.h"

namespace qsy {
    class node
    {
        uint16_t phy_id;
        std::chrono::steady_clock::time_point m_last_keepalive;
        bool m_keepalive_up;
        boost::asio::ip::tcp::socket m_socket;
        blocking_queue<packet> &m_packet_queue;
        static const std::chrono::duration<float> keepalive_threshold;
    public:
        node(uint16_t phy_id, 
            const boost::asio::ip::tcp::endpoint &node_endpoint,
            blocking_queue<packet> &incoming_queue,
            boost::asio::io_service &io_serv);
        node &operator=(node &&other) = default;
        node(node &&other) = default;
        bool keepalive();
        void reset_keepalive();
		void send_command(packet::color_t color = { 0,0,0 }, uint32_t delay = 0);
        ~node();
    };
}
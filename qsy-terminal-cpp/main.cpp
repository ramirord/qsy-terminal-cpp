#include <iostream>
#include "terminal.h"
#include <string>
#include <chrono>
#include <thread>
#include "qsy_packet.h"

const std::string options_str =
"f: Find nodes\n"
"s: Send command\n"
"d: Disconnect and forget nodes\n"
"c: Send command to node\n"
"q: Quit\n";
const std::string prompt_str = "Enter option (h for commands): ";

int main()
{
    qsy::terminal term;

    std::cout << options_str;
    char option = 0;
    std::cout << prompt_str;
    std::cin >> option;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    while (option != 'q') {
        switch (option) {
        case 'f':
            term.find_nodes();
            using namespace std::chrono_literals;
            std::this_thread::sleep_for(2s);
            term.find_nodes_stop();
            break;
        case 's':
            std::cout << "Not yet implemented" << std::endl;
            break;
        case 'h':
            std::cout << options_str;
            break;
		case 'd':
			term.drop_nodes();
			break;
		case 'c':
		{
			int node_phy_id;
			qsy::packet::color_t color(2, 2, 2);
			std::cout << "Enter node no: ";
			std::cin >> node_phy_id;
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			try {
				term.send_command(node_phy_id, color, 0);
			}
			catch (...) {
				std::cerr << "Command transmission failed." << std::endl;
			}

		}
			break;
        default:
            std::cout << "Invalid option." << std::endl;
            break;
        }
        std::cout << prompt_str;
        option = std::cin.get();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    return 0;
}
#include "terminal.h"
#include <iostream>
#include <algorithm>
#include <boost/bind.hpp>

namespace qsy
{
    void terminal::register_nodes()
    {
        auto node_info = m_incoming_nodes.pop();
        while (!node_info.second.is_unspecified()) {
            uint16_t id = node_info.first;
            auto &addr = node_info.second;
            boost::asio::ip::tcp::endpoint node_endpoint(addr, 3000);
            if (nodes.count(id) == 0) {
				std::lock_guard<std::mutex> lock(m_nodes_mutex);
                nodes.emplace(std::make_pair(id, node(id, node_endpoint, m_incoming_packets, m_io_serv)));
				std::cerr << "Registered node no. "
					<< static_cast<int>(id)
					<< " at " << addr << "." << std::endl;
            }
            node_info = m_incoming_nodes.pop();
        }
    }

    void terminal::standby()
    {
		auto packet = m_incoming_packets.pop();
		while (packet.get_type() != packet::packet_type::poison_pill)
		{
			auto packet = m_incoming_packets.pop();
		}
    }

    void terminal::run()
    {
        bool running = true;
        while (running) {
            switch (m_state) {
            case state::discovery:
                register_nodes();
                break;
            case state::running:
            case state::standby:
                standby();
                break;
            case state::exiting:
                running = false;
                break;
            }
        }
    }

    terminal::terminal()
        : m_io_serv(),
		  m_finder(m_incoming_nodes),
          m_running(true),
	      m_nodes_mutex(),
		  m_state(state::standby),
          m_worker(&terminal::run, this),
		  m_io_serv_worker(boost::bind(&boost::asio::io_service::run, &m_io_serv))

    {
    }

    terminal::~terminal()
    {
        m_incoming_packets.push(packet::POISON_PILL);
		m_incoming_nodes.push(std::make_pair(0, boost::asio::ip::address()));
        m_state = state::exiting;
		m_worker.join();
		m_io_serv.stop();
		m_io_serv_worker.join();
    }

	void terminal::send_command(uint16_t phy_id, packet::color_t color, uint32_t delay)
	{
		nodes.at(phy_id).send_command(color, delay);
	}

    void terminal::find_nodes()
    {
        m_state = state::discovery;
        m_finder.start();
    }

    void terminal::find_nodes_stop()
    {
        m_finder.stop();
        m_state = state::standby;
    }

	void terminal::drop_nodes()
	{
		m_state = state::standby;
		nodes.clear();
	}
}


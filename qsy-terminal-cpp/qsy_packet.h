#pragma once
#include <array>
#include <boost/asio.hpp>
#include <cstdint>
#include <iostream>

namespace qsy
{
    class packet {

    public:
        constexpr static size_t PACKET_SIZE = 12;

        struct color_t {
            uint8_t r;
            uint8_t g;
            uint8_t b;
            color_t(uint8_t r = 0, uint8_t g = 0, uint8_t b = 0) : r(r), g(g), b(b) {};
        };

        enum class packet_type { hello, keepalive, touche, command, poison_pill, none };

        explicit packet(packet_type type = packet_type::none,
            uint16_t id = 0,
            color_t &c = color_t(0,0,0),
            uint32_t delay = 0);

        packet::packet(const std::array<char, PACKET_SIZE>& raw_data);

        friend std::ostream& operator<<(std::ostream& os, const packet &p)
        {
            switch (p.m_type) {
            case packet_type::hello:
                os << "** Hello packet**";
                break;
            case packet_type::keepalive:
                os << "** Keepalive packet**";
                break;
            case packet_type::touche:
                os << "** Keepalive packet**";
                break;
            case packet_type::command:
                os << "** Command packet**";
                break;
            }
            os << std::endl;
            os << "Id: " << p.m_id << std::endl;
            os << "Color: (" << (unsigned)p.m_color.r << ", " << (unsigned)p.m_color.g << ", " << (unsigned)p.m_color.b << ")"
                << std::endl;
            os << "Delay: " << p.m_delay << " ms" << std::endl << std::endl;
            return os;
        };

        packet_type get_type() const { return m_type; };
        uint16_t get_id() const { return m_id; };
        const color_t &get_color() const { return m_color; };
        uint32_t get_delay() const { return m_delay; };
		const std::array<char, PACKET_SIZE> &get_data() const { return m_data; };
        static const packet POISON_PILL;
    private:
        packet_type m_type;
        uint16_t m_id;
        color_t m_color;
        uint32_t m_delay;
        std::array<char, PACKET_SIZE> m_data;
    };

}